%% Boosted Random Ferns -BRFs- 2D
%
% Description: 
%   This program computes the boosted random ferns classifier (BRFs) in 
%   order to classify two different classes (positive and negative classes)
%   belonging to a two-dimensional feature space (2D). 
%
%   Particularly, the BRFs classifier is computed using Real AdaBoost in 
%   order to select and combine -automatically- the most discriminative 
%   weak classifiers (WCs) and where each one consists of a specific random 
%   fern [1]. For this 2D demo, each fern is a set of decision stumps 
%   computed at random over the 2D feature space. 
%
%   For more detail about the BRFs classifier refer to references [2][3].
%
% Comments:
%   This program computes the BRFs classifier and compares it against a classical 
%   version of random ferns [1]. This version computes the classifier by selecting 
%   the ferns at random and without AdaBoost.
%
%   The parameters of the classifier and the 2D feature scenario can be found 
%   in the fun_parameters_2d function.
%
%   Four different scenarios to train and test the classifier have been considered, 
%   each one with a particular degree of complexity.
%
%   If you make use of this code for research articles, we kindly encourage
%   to cite the references [2][3], listed below. This code is only for 
%   research and educational purposes.
%
% Steps:
%   Steps to exucute the program:
%     1. Run the prg_setup.m file to configure the program paths.
%     2. Run the prg_brfs_2d.m file to compute the classifier and to perfom 
%        classification on the 2D problem scenario. 
%
% References:
%   [1] Fast keypoint recognition in ten lines of code. M. Ozuysal, P. Fua, V. Lepetit.
%       Computer Vision and Pattern Recognition (CVPR), 2007.
%
%   [2] Bootstrapping Boosted Random Ferns for Discriminative and Efficient Object 
%       Classification. M. Villamizar, J. Andrade-Cetto, A. Sanfeliu and F. Moreno-Noguer.
%       Pattern Recognition, 2012.
%
%   [3] Efficient Rotation Invariant Object Detection using Boosted Random Ferns. 
%       M. Villamizar, F. Moreno-Noguer, J. Andrade-Cetto and A. Sanfeliu. Conference 
%       on Computer Vision and Pattern Recognition (CVPR), San Francisco, USA, June 2010.
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2014
%

%% main function
function prg_brfs_2d()
clc,close all,clear all

% messages
fun_messages('Boosted Random Ferns','presentation');
fun_messages('Boosted Random Ferns','title');

% delete previous variables
fun_delete_variables();

% samples: training and test samples are computed according to the
% scenario example. Samples includes positive and negative samples 
% correponding to a two-class classification problem.
trnSamples = fun_samples_2d('train');
tstSamples = fun_samples_2d('test');

% show samples
fun_show_samples_2d(trnSamples,'train');
fun_show_samples_2d(tstSamples,'test');

% random ferns: this function computes the random ferns which are used
% later to compute the classifier. Here, a fern corresponds to a set of
% decision stumps -binary features- on the 2D feature space.
ferns = fun_random_ferns_2d();

% classifier: random ferns (RFs)
% This function computes the classifier based on classical random ferns.
% This classifier is computed without boostin. The weak classifiers (WCs) 
% -random ferns- are chosen at random to ensemble the strong classifier.
rfs = fun_classifier_rfs_2d(ferns,trnSamples);

% classifier: boosted random ferns (BRFs)
% This function computes the classifier based on boosting random ferns.
% The classifier is computed using real adaboost. The algorithm selects 
% the most discriminative weak classifiers (WCs) -ferns- to ensemble the 
% strong classifier.
brfs = fun_classifier_brfs_2d(ferns,trnSamples);

% results -train samples-
results.train.rfs  = fun_test_2d(rfs,trnSamples);
results.train.brfs = fun_test_2d(brfs,trnSamples);

% results -test samples-
results.test.rfs  = fun_test_2d(rfs,tstSamples);
results.test.brfs = fun_test_2d(brfs,tstSamples);

% show test results
fun_show_classification_results(results.test.rfs,rfs,tstSamples,'RFs - test samples');
fun_show_classification_results(results.test.brfs,brfs,tstSamples,'BRFs - test samples');

% save classifiers and results
fun_data_save(rfs,'./variables/','classifier_rfs.mat');
fun_data_save(brfs,'./variables/','classifier_brfs.mat');
fun_data_save(results,'./variables/','classification_results.mat');

% message
fun_messages('end','title');
end






