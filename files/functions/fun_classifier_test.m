%% test the classifier
% This function tests the classifier on the input sample. The function
% returns the classifier confidence or score over this sample.
function output = fun_classifier_test(clfr,sample)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
numWCs = size(clfr.WCs,1);  % num. weak classifiers
numFeats = size(clfr.WCs,2);  % num. binary features -fern features-

% variables
score = 0;  % classification score

% test weak classifiers (WCs)
for iterWC = 1:numWCs
    
    % fern output associated to the current weak classifier
    z = 1;
    
    % test binary features
    for iterFeat = 1:numFeats
        
        % feature {x1,x2} and threshold (t)
        x = clfr.WCs(iterWC,iterFeat,1);
        t = clfr.WCs(iterWC,iterFeat,2);
        
        % sample value
        value = sample(1,x);
        
        % update output
        if (value>t), z = z + 2^(iterFeat-1); end
        
    end
    
    % update score
    score = score + clfr.hstms(iterWC,z);
    
end

% rough normalization
score = score/numWCs;

% output
output = score;
end