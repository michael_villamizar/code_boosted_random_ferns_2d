%% ferns outputs
% This function computes the outputs of the input random ferns over the 
% input sample. For 2D classification problem, each fern is a set of 
% decision stumps.
function output = fun_fern_outputs_2d(ferns,sample)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
numFerns = size(ferns,1);  % num. random ferns
numFeats = size(ferns,2);  % num. binary features -fern features-

% allocate
data = zeros(1,numFerns);

% random ferns
for iterFern = 1:numFerns
    
    % fern output
    z = 1;
    
    % features
    for iterFeat = 1:numFeats
        
        % feature {x1,x2} and threshold (t)
        x = ferns(iterFern,iterFeat,1);
        t = ferns(iterFern,iterFeat,2);
        
        % sample value
        value = sample(1,x);
        
        % update output
        if (value>t), z = z + 2^(iterFeat-1); end
        
    end
    
    % save
    data(1,iterFern) = z;
    
end

% output
output = data;
end
