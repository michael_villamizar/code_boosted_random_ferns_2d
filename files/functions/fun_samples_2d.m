%% samples
% This function computes the training and test samples according to the
% scenario example. Samples includes positive and negative samples 
% correponding to a two-class classification problem with different degree 
% of complexity.
function output = fun_samples_2d(text)
if (nargin~=1), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters_2d();  % program parameters
example = prms.samples.example;  % scenario example 
numPosSamples = prms.samples.numPosSamples;  % num. positive samples
numNegSamples = prms.samples.numNegSamples;  % num. negative samples

% messages
fun_messages(sprintf('%s samples',text),'process');
fun_messages(sprintf('scenario example: %d',example),'information');
fun_messages(sprintf('numPosSamples: %d',numPosSamples),'information');
fun_messages(sprintf('numNegSamples: %d',numNegSamples),'information');

% load/compute samples
try
    
    % load samples
    samples = fun_data_load('./variables/',sprintf('%s_samples.mat',text));
    
    % messages
    fun_messages('the samples were loaded successfully','information');
    
catch ME
    
    % compute and save samples
    
    % example set
    switch (example)
        case 1
            % samples - example 1
            samples = fun_example_1(numPosSamples,numNegSamples);
        case 2
            % samples - example 2
            samples = fun_example_2(numPosSamples,numNegSamples);
        case 3
            % samples - example 3
            samples = fun_example_3(numPosSamples,numNegSamples);
        case 4
            % samples - example 4
            samples = fun_example_4(numPosSamples,numNegSamples);
        otherwise
            fun_messages('incorrect example set','error');
    end
    
    % save samples
    fun_data_save(samples,'./variables/',sprintf('%s_samples.mat',text));
end

% messages
fun_messages(sprintf('num. samples: %d',size(samples,1)),'information');

% output
output = samples;
end

%% samples: example 1
% This function computes samples for scenario example 1. This example 
% corresponds to two -unimodal- gaussian distributions.
function output = fun_example_1(numPosSamples,numNegSamples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% labels
posLabels = ones(numPosSamples,1);
negLabels = -1*ones(numNegSamples,1);

% clusters: center, sigma and orientation -distributions-
posMean = [.75 .65];
posSigma = [.11 .11];
posTheta = 40*pi/180;
negMean = [.3 .3];
negSigma = [.11 .11];
negTheta = -30*pi/180;

% random samples coordinates
posData = [randn(1,numPosSamples)*posSigma(1); randn(1,numPosSamples)*posSigma(2)];
negData = [randn(1,numNegSamples)*negSigma(1); randn(1,numNegSamples)*negSigma(2)];

% rotation
posMat = [cos(posTheta), -sin(posTheta); sin(posTheta) cos(posTheta)];
negMat = [cos(negTheta), -sin(negTheta); sin(negTheta) cos(negTheta)];
posData = posMat*posData;
negData = negMat*negData;

% traslation
posData(1,:) = posData(1,:) + posMean(1);
posData(2,:) = posData(2,:) + posMean(2);
negData(1,:) = negData(1,:) + negMean(1);
negData(2,:) = negData(2,:) + negMean(2);

% samples
samples = [posData',posLabels; negData',negLabels];

% random order
indexes = randperm(size(samples,1));
samples = samples(indexes,:);

% output
output = samples;
end

%% samples: example 2
% This function computes samples for scenario example 2. This example 
% corresponds to two -multimodal- class samples distributions. For each 
% class, five sample clusters -gaussian distributions- are computed with a 
% very complex layout.
function output = fun_example_2(numPosSamples,numNegSamples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% check num. samples
if (mod(numPosSamples,5)~=0), fun_messages('the number of positive samples must be multiple of five','error'); end
if (mod(numNegSamples,5)~=0), fun_messages('the number of negative samples must be multiple of five','error'); end

% num. positive and negative samples
numPosSamples = numPosSamples/5;
numNegSamples = numNegSamples/5;

% labels
posLabels = ones(numPosSamples,1);
negLabels = -1*ones(numNegSamples,1);

% clusters: center, sigma and orientation -distributions-
posMean1 = [.4 .5];
posMean2 = [.75 .25];
posMean3 = [.25 .9];
posMean4 = [.2 .3];
posMean5 = [.75 .65];
posSigma1 = [.03 .15];
posSigma2 = [.025 .12];
posSigma3 = [.08 .08];
posSigma4 = [.025 .1];
posSigma5 = [.04 .12];
posTheta1 = 0*pi/180;
posTheta2 = 45*pi/180;
posTheta3 = 0*pi/180;
posTheta4 = 0*pi/180;
posTheta5 = -80*pi/180;
negMean1 = [.5 .1];
negMean2 = [.07 .5];
negMean3 = [.2 .65];
negMean4 = [.85 .45];
negMean5 = [.75 .85];
negSigma1 = [.08 .05];
negSigma2 = [.025 .14];
negSigma3 = [.02 .08];
negSigma4 = [.06 .06];
negSigma5 = [.04 .12];
negTheta1 = 0*pi/180;
negTheta2 = 0*pi/180;
negTheta3 = 45*pi/180;
negTheta4 = 0*pi/180;
negTheta5 = -80*pi/180;

% random samples coordinates
posData1 = [randn(1,numPosSamples)*posSigma1(1); randn(1,numPosSamples)*posSigma1(2)];
posData2 = [randn(1,numPosSamples)*posSigma2(1); randn(1,numPosSamples)*posSigma2(2)];
posData3 = [randn(1,numPosSamples)*posSigma3(1); randn(1,numPosSamples)*posSigma3(2)];
posData4 = [randn(1,numPosSamples)*posSigma4(1); randn(1,numPosSamples)*posSigma4(2)];
posData5 = [randn(1,numPosSamples)*posSigma5(1); randn(1,numPosSamples)*posSigma5(2)];
negData1 = [randn(1,numNegSamples)*negSigma1(1); randn(1,numNegSamples)*negSigma1(2)];
negData2 = [randn(1,numNegSamples)*negSigma2(1); randn(1,numNegSamples)*negSigma2(2)];
negData3 = [randn(1,numNegSamples)*negSigma3(1); randn(1,numNegSamples)*negSigma3(2)];
negData4 = [randn(1,numNegSamples)*negSigma4(1); randn(1,numNegSamples)*negSigma4(2)];
negData5 = [randn(1,numNegSamples)*negSigma5(1); randn(1,numNegSamples)*negSigma5(2)];

% rotation
posData1 = [cos(posTheta1), -sin(posTheta1); sin(posTheta1) cos(posTheta1)]*posData1;
posData2 = [cos(posTheta2), -sin(posTheta2); sin(posTheta2) cos(posTheta2)]*posData2;
posData3 = [cos(posTheta3), -sin(posTheta3); sin(posTheta3) cos(posTheta3)]*posData3;
posData4 = [cos(posTheta4), -sin(posTheta4); sin(posTheta4) cos(posTheta4)]*posData4;
posData5 = [cos(posTheta5), -sin(posTheta5); sin(posTheta5) cos(posTheta5)]*posData5;
negData1 = [cos(negTheta1), -sin(negTheta1); sin(negTheta1) cos(negTheta1)]*negData1;
negData2 = [cos(negTheta2), -sin(negTheta2); sin(negTheta2) cos(negTheta2)]*negData2;
negData3 = [cos(negTheta3), -sin(negTheta3); sin(negTheta3) cos(negTheta3)]*negData3;
negData4 = [cos(negTheta4), -sin(negTheta4); sin(negTheta4) cos(negTheta4)]*negData4;
negData5 = [cos(negTheta5), -sin(negTheta5); sin(negTheta5) cos(negTheta5)]*negData5;

% traslation
posData1(1,:) = posData1(1,:) + posMean1(1);
posData1(2,:) = posData1(2,:) + posMean1(2);
posData2(1,:) = posData2(1,:) + posMean2(1);
posData2(2,:) = posData2(2,:) + posMean2(2);
posData3(1,:) = posData3(1,:) + posMean3(1);
posData3(2,:) = posData3(2,:) + posMean3(2);
posData4(1,:) = posData4(1,:) + posMean4(1);
posData4(2,:) = posData4(2,:) + posMean4(2);
posData5(1,:) = posData5(1,:) + posMean5(1);
posData5(2,:) = posData5(2,:) + posMean5(2);
negData1(1,:) = negData1(1,:) + negMean1(1);
negData1(2,:) = negData1(2,:) + negMean1(2);
negData2(1,:) = negData2(1,:) + negMean2(1);
negData2(2,:) = negData2(2,:) + negMean2(2);
negData3(1,:) = negData3(1,:) + negMean3(1);
negData3(2,:) = negData3(2,:) + negMean3(2);
negData4(1,:) = negData4(1,:) + negMean4(1);
negData4(2,:) = negData4(2,:) + negMean4(2);
negData5(1,:) = negData5(1,:) + negMean5(1);
negData5(2,:) = negData5(2,:) + negMean5(2);

% samples
samples = [posData1',posLabels;posData2',posLabels;posData3',posLabels;...
    posData4',posLabels;posData5',posLabels;negData1',negLabels;negData2',negLabels;...
    negData3',negLabels;negData4',negLabels;negData5',negLabels;];

% random order
indexes = randperm(size(samples,1));
samples = samples(indexes,:);

% output
output = samples;
end

%% samples: example 3
% This function computes samples for scenario example 3. This example 
% contains two class distributions with a non-linear layout. The positive
% class distribution -gaussian- is at the space center. whereas the negative 
% distribution presents a ring layout.
function output = fun_example_3(numPosSamples,numNegSamples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% labels
posLabels = ones(numPosSamples,1);
negLabels = -1*ones(numNegSamples,1);

% samples properties: distance and orientation from center
negData = 0.38 + 0.1*sign(randn(1,numNegSamples)).*rand(1,numNegSamples);
posData = 0.0 + 0.25*sign(randn(1,numPosSamples)).*rand(1,numPosSamples);
posTheta = 2*pi*rand(1,numPosSamples);
negTheta = 2*pi*rand(1,numNegSamples);

% coordinates
posX = 0.5 + cos(posTheta).*posData;
posY = 0.5 + sin(posTheta).*posData;
negX = 0.5 + cos(negTheta).*negData;
negY = 0.5 + sin(negTheta).*negData;

% samples
samples = [posX',posY',posLabels;negX',negY',negLabels];

% random order
indexes = randperm(size(samples,1));
samples = samples(indexes,:);

% output
output = samples;
end

%% samples: example 4
% This function computes samples for scenario example 4. This example 
% contains two class sample distributions with a spiral layout.
function output = fun_example_4(numPosSamples,numNegSamples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
start = 90;         % determines how far from the origin the spirals start, in degrees
noise = 2.0;        % displaces the instances from the spiral
degrees = 350;      % controls the length of the spirals

% degrees
deg2rad = (2*pi)/360;
start = start * deg2rad;

% spiral 1
theta = start + sqrt(rand(numPosSamples,1)) * degrees * deg2rad;
d1 = [-cos(theta).*theta + rand(numPosSamples,1)*noise, sin(theta).*theta + rand(numPosSamples,1)*noise, -1*ones(numPosSamples,1)];

% spiral 2
theta = start + sqrt(rand(numNegSamples,1)) * degrees * deg2rad;
d2 = [cos(theta).*theta + rand(numNegSamples,1)*noise, -sin(theta).*theta + rand(numNegSamples,1)*noise, +1*ones(numNegSamples,1)];

% samples
samples = [d1;d2];

% normalization
tmp = samples(:,1:2);
tmp = tmp - min(tmp(:));
tmp = tmp./max(tmp(:));
samples(:,1:2) = tmp;

% random order
indexes = randperm(size(samples,1));
samples = samples(indexes,:);

% output
output = samples;
end

