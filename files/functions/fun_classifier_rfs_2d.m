%% classifier: random ferns (RFs)
% This function computes the classifier based on classical random ferns.
% This classifier is computed without boosting. The weak classifiers (WCs) 
% -random ferns- are chosen at random to ensemble the strong classifier.
function output = fun_classifier_rfs_2d(ferns,samples)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% message
fun_messages('classifier: random ferns (RFs)','process');

% parameters
prms = fun_parameters_2d();  % program parameters
eps = prms.classifier.eps;  % epsilon
numWCs = prms.classifier.numWCs;  % num. weak classifiers -selected ferns-
numFerns = prms.classifier.numFerns;  % num. random ferns -sets of decision stumps-
numFeats = prms.classifier.numFeats;  % num. binary features -decision stump-     
numBins = 2^numFeats;  % num. histogram bins   

% num. samples
numSamples = size(samples,1);

% allocate
ratHstms = zeros(numWCs,numBins);  % ratio of weak classifiers probabilities
fernOutputs = zeros(numSamples,numWCs);  % fern outputs

% weak classifiers (WCs): the weak classifiers used to 
% emsemble the classifier are chosen randomly. Each weak 
% classifier corresponds to a specific random fern.
indxs = min(numFerns,ceil(numFerns*rand(numWCs,1)));
WCs = ferns(indxs,:,:);

% fern outputs: this computes the fern outputs associated 
% to the weak classifiers (WCs).
for iterSample = 1:numSamples
    fernOutputs(iterSample,:) = fun_fern_outputs_2d(WCs,samples(iterSample,:));
end

% weak classifiers: the algorithm computes the weak 
% classifiers probabilities using the input samples
% and the fern outputs on the samples. The weak 
% classifiers were chosen previously -at random-.
for iterWC = 1:numWCs

    % allocate
    posHstm = zeros(1,numBins);  % weak classifier probability for positive class
    negHstm = zeros(1,numBins);  % weak classifier probability for negative class
    
    % samples
    for iterSample = 1:numSamples
        
        % sample label
        label = samples(iterSample,3);
        
        % fern output associated with current weak classifier
        z = fernOutputs(iterSample,iterWC);
        
        % update probabilities
        if (label==-1), negHstm(1,z) = negHstm(1,z) + 1; end 
        if (label==+1), posHstm(1,z) = posHstm(1,z) + 1; end
        
    end
    
    % normalization
    posHstm = posHstm./sum(posHstm(:));
    negHstm = negHstm./sum(negHstm(:));
        
    % log. ratio of class probabilities
    ratHstms(iterWC,:) = 0.5*log((posHstm + eps)./(negHstm + eps)); 
    
    % message
    if (mod(iterWC,10)==0), fun_messages(sprintf('weak classifier -> %d/%d',iterWC,numWCs),'information'); end
    
end

% classifier: random ferns
clfr.WCs = WCs;  % weak classifiers (WCs) data -selected ferns-
clfr.prms = prms;  % program parameters
clfr.hstms = ratHstms;  % weak classifiers (WCs) probabilities -log. ratio-
clfr.ferns = ferns;  % random ferns -pool of random ferns-

% output
output = clfr;
end
