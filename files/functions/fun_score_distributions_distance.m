%% distance between positive and negative score distributions
% This function computes the distance between the positive and negative
% score distributions -gaussians-.
function output = fun_score_distributions_distance(posMiu,negMiu,posVar,negVar)
if (nargin~=4), fun_messages('incorrect input variables','error'); end

% Hellinger distance
dst = fun_hellinger_distance(posMiu,negMiu,posVar,negVar);

% check empty distributions
if(posVar==0 || negVar==0), dst = 0; end

% output
output = dst;
end

%% Hellinger distance
% This function computes the Hellinger disatnce between two gaussian
% distributions.
function output = fun_hellinger_distance(posMiu,negMiu,posVar,negVar)
if (nargin~=4), fun_messages('incorrect input variables','error'); end

% Hellinger distance -> [0,1]: 
k1 = 2*sqrt(posVar)*sqrt(negVar);
k2 = posVar + negVar;
k3 = (posMiu-negMiu)^2;
dst = 1 - sqrt(k1/k2)*exp(-0.25*k3/k2);

% output
output = dst;
end
