%% test step
% This function tests the classifier on the input samples in order to 
% measure its classification performance. 
function output = fun_test_2d(clfr,samples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% variables
labels = samples(:,3);  % sample labels
numSamples = size(samples,1);  % num. samples

% allocate
scores = zeros(1,numSamples);  % samples scores

% Sample scores: the classifier is tested on the input samples to compute
% the sample scores or classifier confidence over these samples
for iterSample = 1:numSamples
    scores(1,iterSample) = fun_classifier_test(clfr,samples(iterSample,1:2));
end

% positive and negative scores
posScores = scores(find(labels>0));
negScores = scores(find(labels<0));

% score -gaussian- distributions
posMiu = mean(posScores);  % positive class mean
negMiu = mean(negScores);  % negative class mean
posVar = std(posScores)^2;  % positive class variance
negVar = std(negScores)^2;  % negative class variance

% score distribution distances: this computes the distance between the
% positive and negative score distributions, indicating the degree of 
% separability between these two classes.
dst = fun_score_distributions_distance(posMiu,negMiu,posVar,negVar);

% classification rates: this function computes some classification 
% rates of the classifier over the input samples.
clfRates = fun_classification_rates(scores,labels);

% classification results
results.eer = clfRates.eer;  % equal error rate (EER)
results.thr = clfRates.thr;  % classification threshold to obain EER
results.idx = clfRates.idx;  % threshold index -EER index-
results.dst = dst;  % distribution distance
results.curves.rec = clfRates.rec;  % recall curve
results.curves.pre = clfRates.pre;  % precision curve
results.curves.fme = clfRates.fme;  % f-measure curve
results.curves.tps = clfRates.tps;  % true positives curve
results.curves.fps = clfRates.fps;  % false positives curve
results.curves.fns = clfRates.fns;  % false negatives curve
results.distrs.posMiu = posMiu;  % positive class mean
results.distrs.negMiu = negMiu;  % negative class mean
results.distrs.posVar = posVar;  % positive class variance
results.distrs.negVar = negVar;  % negative class variance
results.scores.samples = scores; % classification scores
results.scores.positive = posScores;  % positive scores
results.scores.negative = negScores;  % negative scores

% output
output = results;
end

%% classification rates
% This function computes some classification rates of the classifier over 
% samples. The function uses the sample scores -classifier confidence- with
% the sample labels to compute these classification rates. Specifically, 
% the function computes the precision, recall and f-measure plots, and the
% equal error rate (EER), that is the point where recall and precision are
% equal. The function also computes true positives, false positives and
% false negatives rates.
function output = fun_classification_rates(scores,labels)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% positive and negative indexes
posIndxs = find(labels==+1);
negIndxs = find(labels==-1);

% variables
eer = 0;  % equal error rate
mrt = inf;  % max. rate
thr = 0;  % classification threshold
cnt = 0;  % counter 
idx = 0;  % index

% allocate
rec = zeros(1,1000);  % recall
pre = zeros(1,1000);  % precision
fme = zeros(1,1000);  % f-measure
tps = zeros(1,1000);  % true positives
fps = zeros(1,1000);  % false positives
fns = zeros(1,1000);  % false negatives

% min. and max. threshold values
maxThr = max(scores);  % max. threshold
minThr = min(scores);  % min. threshold

% threshold step
stepThr = (maxThr-minThr)/(1000-1);

% threshold
for iterThr = maxThr:-stepThr:minThr
   
    % counter
    cnt = cnt + 1;
    
    % rates
    tp = sum(scores(posIndxs)>iterThr,2);   % true positives
    fp = sum(scores(negIndxs)>iterThr,2);   % false positives
    fn = sum(scores(posIndxs)<=iterThr,2);  % false negatives
    
    % classification rates
    r = tp/(tp + fn);  % recall
    p = tp/(tp + fp);  % precision
    f = 2*r*p/(r+p);  % f-measure
    
    % dif. recall-precision rate
    rate = abs(r-p);
    
    % best rate
    if (rate<mrt && r~=0 && p~=0)
        % update values
        mrt = rate;  % max. rate
        thr = iterThr;  % threshold
        eer = (r+p)/2;  % equal error rate
        idx = cnt;  % index
    end
    
    % save values
    tps(cnt) = tp;
    fps(cnt) = fp;
    fns(cnt) = fn;
    rec(cnt) = r;
    pre(cnt) = p;
    fme(cnt) = f;
    
end

% check
if (eer==0), fun_messages('eer value is zero','warning'); end

% classification data
data.eer = eer;  % equal error rate (EER)
data.thr = thr;  % classification threshold to achieve EER
data.idx = idx;  % EER rate index
data.rec = rec;  % recall rates
data.pre = pre;  % precision rates
data.fme = fme;  % f-measure rates
data.tps = tps;  % true positives
data.fps = fps;  % false positives
data.fns = fns;  % false negatives

% output
output = data;
end


