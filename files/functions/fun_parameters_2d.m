%% parameters
% This function returns the program parameters.
function output = fun_parameters_2d()

% classifier
eps = 0.001;                            % epsilon value
numWCs = 50;                            % num. weak classifiers -selected ferns-
numFeats = 6;                           % num. binary features -decision stumps-
numFerns = 1000;                        % num. random ferns -sets of decision stumps-
classifier = struct('eps',eps,'numWCs',numWCs,'numFeats',numFeats,'numFerns',numFerns);

% samples
example = 4;                            % scenario example
numPosSamples = 500;                    % num. positive samples
numNegSamples = 500;                    % num. negative samples
samples = struct('example',example,'numPosSamples',numPosSamples,'numNegSamples',numNegSamples);

% visualization
fontSize = 10;                          % font width
distSize = 100;                         % distribution size -num. bins-
posColor = [1,0.7,0];                 % color for positive samples
negColor = [0,0,1];                     % color for negative samples
gridSize = 100;                         % grid size
lineWidth = 4;                          % line width
markerSize = 16;                        % marker size
visualization = struct('distSize',distSize,'markerSize',markerSize,'lineWidth',lineWidth,...
    'fontSize',fontSize,'gridSize',gridSize,'posColor',posColor,'negColor',negColor);

% parameters
prms.samples = samples;                 % samples parameters
prms.classifier = classifier;           % classifier parameters
prms.visualization = visualization;     % visualization parameters

% output
output = prms;
end
