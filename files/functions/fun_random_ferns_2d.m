% random ferns
% This function computes the random ferns which are used later to compute 
% the classifier. Here, a fern corresponds to a set of decision stumps 
% -binary features- on the 2D feature space.
function output = fun_random_ferns_2d()

% parameters
prms = fun_parameters_2d();             % program parameters
numFerns = prms.classifier.numFerns;    % classifier: num. random ferns
numFeats = prms.classifier.numFeats;    % classifier: num. features

% messages
fun_messages('random ferns','process');
fun_messages(sprintf('num. ferns: %d',numFerns),'information');
fun_messages(sprintf('num. features: %d',numFeats),'information');

% check
try
    
    % load random ferns
    ferns = fun_data_load('./variables/','ferns.mat');
    
    % messages
    fun_messages('the random ferns were loaded successfully','information');
        
catch ME
    
    % allocate
    ferns = zeros(numFerns,numFeats,2);
    
    % ferns computation: each fern is a set of binary features, and each
    % feature is a decision stump in the 2D space and defined by an axe and
    % threshold. These parameters are computed at random.
    for iterFern = 1:numFerns
        for iterFeat = 1:numFeats
            % feature: axis (x1,x2) and threshold (t)
            x = min(2,ceil(rand*2));
            t = rand;
            % save
            ferns(iterFern,iterFeat,1) = x;
            ferns(iterFern,iterFeat,2) = t;
        end
    end
    
    % save ferns
    fun_data_save(ferns,'./variables/','ferns.mat');
    
end

% output
output = ferns;
end
