%% classifier: boosted random ferns (BRFs)
% This function computes the classifier based on boosting random ferns. The
% method makes use of real adabbost to select the most discriminative ferns
% and to compute weak classifiers (WCs). Each weak classifier corresponds
% to a particular random fern. The classifier results in the combination 
% of weak classifiers -chosen ferns-.
function output = fun_classifier_brfs_2d(ferns,samples)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% message
fun_messages('classifier: boosted random ferns (BRFs)','process');

% parameters
prms = fun_parameters_2d();  % program parameters
eps = prms.classifier.eps;  % epsilon
numWCs = prms.classifier.numWCs;  % num. weak classifiers -selected ferns-
numFerns = prms.classifier.numFerns;  % num. random ferns -sets of decision stumps-
numFeats = prms.classifier.numFeats;  % num. binary features -decision stump-     
numBins = 2^numFeats;  % num. histogram bins   

% num. samples
numSamples = size(samples,1);

% sample labels
labels = samples(:,3)';

% sample weights
weights = (1/numSamples)*ones(1,numSamples);

% allocate
WCs = zeros(numWCs,numFeats,2); % weak classifiers (WCs) data
indexes = zeros(1,numWCs);  % weak clasifiers indxes -chosen ferns-
ratHstms = zeros(numWCs,numBins);  % ratio of weak classifiers probabilities
fernOutputs = zeros(numSamples,numFerns);  % fern outputs on the input samples

% fern outputs: this computes the fern outputs associated 
% to the weak classifiers (WCs) on the input samples.
for iterSample = 1:numSamples
    fernOutputs(iterSample,:) = fun_fern_outputs_2d(ferns,samples(iterSample,:));
end

% boosting: in each iteration the algorithm selects the most relevant fern
% in order to compute a weak classifier (WC). The combination of weak
% classifiers defines the strong classifier.
for iterWC = 1:numWCs

    % variables
    conf = zeros(1,numSamples); % weak classifier confidence
    index = 0;  % selected fern index
    maxDst = inf;  % max. distance between class distributions
    ratHstm = zeros(1,numBins);  % ratio of class probabilities
        
    % random ferns pool
    for iterFern = 1:numFerns
        
        % weak classifier probabilities
        posHstm = zeros(1,numBins);  % for the positive class
        negHstm = zeros(1,numBins);  % for the negative class
        
        % compute weak classifier probabilities
        for iterSample = 1:numSamples
            
            % sample label
            label = labels(iterSample);
        
            % fern output
            z = fernOutputs(iterSample,iterFern);
            
            % update weak classifier probabilities according to the sample
            % weight.
            if (label==+1), posHstm(1,z) = posHstm(1,z) + weights(1,iterSample); end
            if (label==-1), negHstm(1,z) = negHstm(1,z) + weights(1,iterSample); end
            
        end
        
        % histogram normalization
        posHstm = posHstm./sum(posHstm(:));
        negHstm = negHstm./sum(negHstm(:));
        
        % battacharyya distance: the distance between the positive and 
        % negative class distributions -histomgrams- are measure using the
        % battacharyya coefficient.
        dst = 2*sum(sqrt((posHstm.*negHstm)),2);
        
        % best weak classifier: the weak classifier that reduces best the
        % Battacharyya distance and that the chosen fern is not repeated.
        if (dst<maxDst) && (sum(iterFern==indexes,2)==0)
                        
            % update valeus
            index = iterFern;  % select fern index
            maxDst  = dst;  % max. distance
            
            % log. ratio of class probabilities
            ratHstm = 0.5*log((posHstm + eps)./(negHstm + eps)); 
           
        end
    end
    
    % check
    if (maxDst==inf), fun_messages('weak classifier not found ','error'); end
 
    % weak classifier confidence: the computed weak classifier is tested on
    % the samples to obtain the classifier confidence or classification score.
    for iterSample = 1:numSamples
        % fern output
        z = fernOutputs(iterSample,index);
        % confidence
        conf(1,iterSample) = ratHstm(1,z);
    end
            
    % update weights
    weights = weights.*exp(-1*labels.*conf);
    
    % naive step to remove high weights due to outliers
    weights = min(weights,10/numSamples);
    
    % normalization
    weights = weights./(sum(weights(:)));
    
    % update weak classifier indexes
    indexes(1,iterWC) = index;  
        
    % save weak classifier
    WCs(iterWC,:,:) = ferns(index,:,:);  % weak classifier data  
    ratHstms(iterWC,:) = ratHstm;  % weak classifier probability -ratio-
    
    % message
    if (mod(iterWC,10)==0), fun_messages(sprintf('weak classifier -> %d/%d',iterWC,numWCs),'information'); end
    
end

% classifier: boosted random ferns
clfr.WCs = WCs;  % weak classifiers (WCs) data -selected ferns-
clfr.prms = prms;  % program parameters
clfr.hstms = ratHstms;  % weak classifiers (WCs) probabilities -log. ratio-
clfr.ferns = ferns;  % random ferns -pool of random ferns-

% output
output = clfr;
end
