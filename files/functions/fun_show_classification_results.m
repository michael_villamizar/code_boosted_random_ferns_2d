%% classification results
% This function shows the classification results on the input samples.
function fun_show_classification_results(results,clfr,samples,text)

% parameters
prms = fun_parameters_2d();  % program parameters
fs = prms.visualization.fontSize;  % visualization: font size
lw = prms.visualization.lineWidth;  % visualization: line width
ms = prms.visualization.markerSize;  % visualization: marker size
pc = prms.visualization.posColor;  % visualization: positive color
nc = prms.visualization.negColor;  % visualization: negative color
ds = prms.visualization.distSize;  % visualization: distribution size      

% results
eer = results.eer;  % equal error rate
thr = results.thr;  % classification threshold
ind = results.idx;  % threshold index
dst = results.dst;  % distributions distance
rec = results.curves.rec;  % recall curve
pre = results.curves.pre;  % precision curve
fme = results.curves.fme;  % f-measure curve
tps = results.curves.tps;  % true positives curve
fps = results.curves.fps;  % false positives curve        
fns = results.curves.fns;  % false negatives curve
posMiu = results.distrs.posMiu;  % positive class mean
negMiu = results.distrs.negMiu;  % negative class mean
posVar = results.distrs.posVar;  % positive class variance
negVar = results.distrs.negVar;  % negative class variance
scores = results.scores.samples;  % classification scores
posScores = results.scores.positive;  % negative scores
negScores = results.scores.negative;  % positive scores

% variables
labels = samples(:,3);  % sample labels
numSamples = size(samples,1);  % num. samples

%% messages
fun_messages(sprintf('Test: %s results',text),'process');
fun_messages(sprintf('EER -> %.3f',eer),'information');
fun_messages(sprintf('threshold -> %.3f',thr),'information');
fun_messages(sprintf('distribution distance -> %.3f',dst),'information');

%% show precision-recall plot
figure,subplot(231),plot(rec,pre,'g-','linewidth',lw),grid on,hold on;
line([0,1],[0,1]); 
title('Precision-Recall Plot','fontsize',fs);
xlabel('Recall','fontsize',fs),ylabel('Precision','fontsize',fs);
legend(sprintf('EER: %.3f',eer));

%% show positive and negative class scores
subplot(232),grid on,hold on;
plot(posScores,'-o','color',pc,'linewidth',lw),hold on;
plot(negScores,'-*','color',nc,'linewidth',lw), hold on
title('Score Performance','fontsize',fs),hold on
xlabel('# Samples','fontsize',fs),ylabel('Score','fontsize',fs);
legend('Positive','Negative');

%% show positive and negative score distributions 

% min. and max. score values
minScore = min(scores(:));
maxScore = max(scores(:));

% variables
k = sqrt(2*pi);  % constant
s = minScore:inv(ds-1):maxScore;    % scores

% positive and negative gaussian distributions
p = 1/(ds*k*sqrt(posVar))*exp(-0.5*((s-posMiu).^2)./posVar);
n = 1/(ds*k*sqrt(negVar))*exp(-0.5*((s-negMiu).^2)./negVar);

% plot score distributions
subplot(233),plot(s,p,'-o','color',pc,'linewidth',lw), hold on;
plot(s,n,'-*','color',nc,'linewidth',lw'), hold on;

% messages
title(sprintf('Distributions Distance: %.3f',dst),'fontsize',fs);
xlabel('Score','fontsize',fs),ylabel('Probability','fontsize',fs);
%legend(sprintf('Pos. - mean %.2f - var. %.4f',posMiu,posVar),...
%        sprintf('Neg. - mean %.2f - var. %.4f',negMiu,negVar));
legend('Pos. Distribution','Neg. Distribution')
%% show classification results

% figure
subplot(234),axis([0,1,0,1]),hold on,grid on

% samples
for iterSample = 1:numSamples
    
    % sample properties
    lbl = labels(iterSample);   % true label
    scr = scores(iterSample);   % score
    est = sign(2*(scr>thr)-1);  % estimated label
    
    % plot sample
    if (lbl>0)
        if (lbl==est)
            % true positive
            plot(samples(iterSample,2),samples(iterSample,1),'+','color',pc,...
                'markersize',ms,'linewidth',lw),hold on
        else
            % false negative
            plot(samples(iterSample,2),samples(iterSample,1),'+','color',[0,0,0],...
                'markersize',ms,'linewidth',lw),hold on
        end
    else
        if (lbl==est)
            % true negative
            plot(samples(iterSample,2),samples(iterSample,1),'o','color',nc,...
                'markersize',ms,'linewidth',lw),hold on
        else
            % false positive
            plot(samples(iterSample,2),samples(iterSample,1),'o','color',[0,0,0],...
                'markersize',ms,'linewidth',lw),hold on
        end
    end
end

% messages
title('Classification Results','fontsize',fs), hold on;
xlabel(sprintf('Recall: %.2f - Precision: %.2f',rec(ind),pre(ind)),'fontsize',fs), hold on;
%legend('Positive','Negative');

%% show classification and uncertainty maps

% variable
entThr = 0.9;   % entropy threshold

% compute classification and uncertainty maps
[clfMap,uncMap] = fun_classification_maps_2d(clfr);

% figure: classification map
subplot(235),imagesc(clfMap,[0,1]), hold on;
xlabel('Sample Space','fontsize',fs);
title('Classification Map','fontsize',fs), hold on;

% figure: uncertainty map
subplot(236),imagesc(uncMap,[0,1]), hold on;
contour(rgb2gray(uncMap),entThr,'linewidth',lw), hold on;
xlabel(sprintf('Entropy boundary -> %.2f',entThr),'fontsize',fs);
title('Uncertainty Map','fontsize',fs), hold on;

end
